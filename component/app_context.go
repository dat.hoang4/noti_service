package component

import (
	"github.com/go-redis/redis/v8"
	"gorm.io/gorm"
)

type AppContext interface {
	GetMainDBConnection() *gorm.DB
	GetRedisConnection() *redis.Client
}

type appCtx struct {
	db    *gorm.DB
	redis *redis.Client
}

func NewAppContext(
	db *gorm.DB,
	redis *redis.Client,
) *appCtx {
	return &appCtx{db: db, redis: redis}
}

func (ctx *appCtx) GetMainDBConnection() *gorm.DB {
	return ctx.db
}

func (ctx *appCtx) GetRedisConnection() *redis.Client {
	return ctx.redis
}


FROM golang:1.18 AS builder

COPY . /go/src/backend_api

WORKDIR /go/src/backend_api
RUN GOPROXY=https://goproxy.cn mkdir -p bin/ && go build -o ./bin/ ./...
WORKDIR ./bin

FROM debian:stable-slim

RUN apt-get update && apt-get install -y --no-install-recommends \
        ca-certificates  \
        netbase \
        && rm -rf /var/lib/apt/lists/ \
        && apt-get autoremove -y && apt-get autoclean -y

COPY --from=builder /go/src/backend_api/bin ./app
COPY --from=builder /go/src/backend_api/.env ./app/.env
COPY --from=builder /go/src/backend_api/ssl ./app/ssl
COPY --from=builder /go/src/backend_api/db_data ./app/db_data

WORKDIR ./app

EXPOSE 8000
CMD ["./notification_service"]
#ENTRYPOINT ["-env", "release"]
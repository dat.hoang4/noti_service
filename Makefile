all: run

run:
	go run main.go

start:
	docker compose up --build -d

stop:
	docker compose down

gen-cal:
	protoc --go_out=. --go_opt=paths=source_relative \
        --go-grpc_out=. --go-grpc_opt=paths=source_relative \
        proto/notification.proto

run_grpc_server:
	go run grpc/server.go
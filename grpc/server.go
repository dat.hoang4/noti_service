package grpc

import (
	"fmt"
	"github.com/joho/godotenv"
	"github.com/twilio/twilio-go"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	notification "notification_service/proto"
	"os"

	"log"
	"net"
)

type server struct{}

var Client *twilio.RestClient

func RunGrpcServer() {

	lis, err := net.Listen("tcp", "0.0.0.0:50069")
	if err != nil {
		log.Fatalf("err while create listen %v", err)
	}

	certFile := "ssl/server.crt"
	keyFile := "ssl/server.key"

	creds, sslErr := credentials.NewServerTLSFromFile(certFile, keyFile)
	if sslErr != nil {
		log.Fatalf("create creds ssl err %v\n", sslErr)
		return
	}
	opts := grpc.Creds(creds)

	s := grpc.NewServer(opts)

	notification.RegisterNotificationServer(s, &server{})

	fmt.Println("GRPC server is running...")

	err = s.Serve(lis)

	if err != nil {
		log.Fatalf("err while serve %v", err)
	}
}

func init() {
	err := godotenv.Load(".env")
	if err != nil {
		fmt.Printf("loi luc init nhe: %s", err.Error())
	}
	Username := os.Getenv("ACCOUNT_SID")
	Password := os.Getenv("AUTH_TOKEN")
	Client = twilio.NewRestClientWithParams(twilio.ClientParams{
		Username: Username,
		Password: Password,
	})

}

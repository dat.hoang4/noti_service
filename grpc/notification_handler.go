package grpc

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"github.com/sendgrid/sendgrid-go"
	"github.com/sendgrid/sendgrid-go/helpers/mail"
	"github.com/slack-go/slack"
	openapi "github.com/twilio/twilio-go/rest/api/v2010"
	"io"
	"log"
	"net/http"
	notification "notification_service/proto"
	"os"
)

func (s *server) SendMail(ctx context.Context, req *notification.SendMailReq) (*notification.NotificationRes, error) {
	from := mail.NewEmail("Tira", "hoaiduc2304@gmail.com") // Change to your verified sender
	subject := "Sending with Twilio SendGrid is Fun"
	to := mail.NewEmail("Sou", req.To) // Change to your recipient
	plainTextContent := "and easy to do anywhere, even with Go"
	htmlContent := "<strong>and easy to do anywhere, even with Go</strong>"
	message := mail.NewSingleEmail(from, subject, to, plainTextContent, htmlContent)
	client := sendgrid.NewSendClient(os.Getenv("SENDGRID_KEY"))
	_, err := client.Send(message)
	if err != nil {
		return &notification.NotificationRes{Success: false}, err
	}
	return &notification.NotificationRes{Success: true}, nil
}

func (s *server) SendSMS(ctx context.Context, req *notification.SendSMSReq) (*notification.NotificationRes, error) {
	phone := fmt.Sprintf("%s", req.To)
	params := openapi.CreateMessageParams{}
	params.SetTo(phone)
	params.SetFrom(os.Getenv("FROM_TWILIO_NUMBER"))
	params.SetBody("ok roai nha\n")

	_, err := Client.Api.CreateMessage(&params)
	if err != nil {
		return &notification.NotificationRes{
			Success: false,
		}, err
	}
	return &notification.NotificationRes{
		Success: true,
	}, nil
}

func (s *server) SendWhatsapp(ctx context.Context, req *notification.SendWhatsappReq) (*notification.NotificationRes, error) {
	phone := fmt.Sprintf("whatsapp:%s", req.To)
	params := openapi.CreateMessageParams{}
	params.SetTo(phone)
	params.SetFrom(os.Getenv("FROM_WHATAPP_NUMBER"))
	params.SetBody("ok roai nha\n")

	_, err := Client.Api.CreateMessage(&params)
	if err != nil {
		fmt.Printf("Send noti for whatsapp channel fail: %s\n", err.Error())
		return &notification.NotificationRes{
			Success: false,
		}, err
	}

	return &notification.NotificationRes{
		Success: true,
	}, nil
}

func (s *server) SendSlack(ctx context.Context, req *notification.SendSlackReq) (*notification.NotificationRes, error) {
	OAUTH_TOKEN := "xoxb-1594563196311-3836175069382-dkfBHNq8faF6eIxaXzFUrid5" // Paste your bot user token here

	api := slack.New(OAUTH_TOKEN)
	attachment := slack.Attachment{
		Pretext: "Pretext",
		Text:    "Hello from GolangDocs!",
	}

	channelId, timestamp, err := api.PostMessage(
		req.To,
		slack.MsgOptionText("This is the main message", false),
		slack.MsgOptionAttachments(attachment),
		slack.MsgOptionAsUser(true),
	)
	if err != nil {
		fmt.Printf("Send noti for whatsapp channel fail: %s\n", err.Error())
		return &notification.NotificationRes{
			Success: false,
		}, err
	}

	log.Printf("Message successfully sent to Channel %s at %s\n", channelId, timestamp)
	return &notification.NotificationRes{
		Success: true,
	}, nil
}

type Message struct {
	ChatID int64  `json:"chat_id"`
	Text   string `json:"text"`
}

func (s *server) SendTele(ctx context.Context, req *notification.SendTeleReq) (*notification.NotificationRes, error) {
	payload, err := json.Marshal(&Message{
		ChatID: req.GetToNumber(),
		Text:   "acccccccccc",
	})
	if err != nil {
		return &notification.NotificationRes{
			Success: false,
		}, err
	}
	response, err := http.Post("https://api.telegram.org/bot5538851501:AAHv-l2sOE_E4gL-rogbkQcrPZiTZuoZ5mw/sendMessage", "application/json", bytes.NewBuffer(payload))
	if err != nil {
		return &notification.NotificationRes{
			Success: false,
		}, err
	}
	defer func(body io.ReadCloser) {
		if err := body.Close(); err != nil {
			log.Println("failed to close response body")
		}
	}(response.Body)
	if response.StatusCode != http.StatusOK {
		return &notification.NotificationRes{
			Success: false,
		}, err
	}
	log.Printf("Message successfully sent to %v", req.GetToNumber())
	return &notification.NotificationRes{
		Success: true,
	}, nil
}

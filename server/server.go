package server

import (
	"context"
	"errors"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/go-redis/redis/v8"
	"gorm.io/gorm"
	"log"
	"net/http"
	notification "notification_service/proto"
	"os/signal"
	"syscall"
	"time"
)

type Server struct {
	ServerReady        chan bool
	Port               int
	AppEnv             string
	DBConn             *gorm.DB
	RedisConn          *redis.Client
	NotificationClient notification.NotificationClient
}

// Start start http server
func (s *Server) Start() {

	// Create context that listens for the interrupt signal from the OS.
	// Reference: https://github.com/gin-gonic/examples/blob/master/graceful-shutdown/graceful-shutdown/notify-with-context/server.go
	ctx, stop := signal.NotifyContext(context.Background(), syscall.SIGINT, syscall.SIGTERM)
	defer stop()

	gin.SetMode(gin.ReleaseMode)
	r := gin.New()
	r.Use(gin.Recovery())

	if s.AppEnv == "dev" {
		gin.SetMode(gin.DebugMode)
		r.Use(gin.Logger())
	}

	//_ := component.NewAppContext(s.DBConn, s.RedisConn)

	v1 := r.Group("/api/v1")
	v1.POST("/mail", s.SendMail())
	v1.POST("/twilio", s.SendSMSTwilio())
	v1.POST("/whatsapp", s.SendWhatsappSMS())
	v1.POST("/slack", s.SendSlack())
	v1.POST("/tele", s.SendTele())

	srv := &http.Server{
		Addr:    fmt.Sprintf(":%d", s.Port),
		Handler: r,
	}

	// Initializing the server in a goroutine so that
	// it won't block the graceful shutdown handling below
	go func() {
		log.Printf("Server run on PORT :%d\n", s.Port)
		if err := srv.ListenAndServe(); err != nil && errors.Is(err, http.ErrServerClosed) {
			log.Printf("listen: %s\n", err)
		}
	}()

	if s.ServerReady != nil {
		s.ServerReady <- true
	}

	// Listen for the interrupt signal.
	<-ctx.Done()

	// Restore default behavior on the interrupt signal and notify user of shutdown.
	stop()
	log.Println("shutting down gracefully, press Ctrl+C again to force")

	// The context is used to inform the server it has 5 seconds to finish
	// the request it is currently handling
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		log.Fatal("Server forced to shutdown: ", err)
	}

	log.Println("Server exiting")
}

type NotiReq struct {
	To       string `json:"to"`
	ToNumber int64  `json:"toNumber"`
}

func (s *Server) SendMail() gin.HandlerFunc {
	return func(c *gin.Context) {
		var NotiReq NotiReq
		err := c.BindJSON(&NotiReq)
		if err != nil {
			return
		}
		s.NotificationClient.SendMail(context.Background(), &notification.SendMailReq{
			To: NotiReq.To,
		})
	}
}

func (s *Server) SendSMSTwilio() gin.HandlerFunc {
	return func(c *gin.Context) {
		var NotiReq NotiReq
		err := c.BindJSON(&NotiReq)
		if err != nil {
			return
		}
		s.NotificationClient.SendSMS(context.Background(), &notification.SendSMSReq{
			To: NotiReq.To,
		})
	}
}

func (s *Server) SendWhatsappSMS() gin.HandlerFunc {
	return func(c *gin.Context) {
		var NotiReq NotiReq
		err := c.BindJSON(&NotiReq)
		if err != nil {
			return
		}
		s.NotificationClient.SendWhatsapp(context.Background(), &notification.SendWhatsappReq{
			To: NotiReq.To,
		})
	}
}

func (s *Server) SendTele() gin.HandlerFunc {
	return func(c *gin.Context) {
		var NotiReq NotiReq
		err := c.BindJSON(&NotiReq)
		if err != nil {
			return
		}
		s.NotificationClient.SendTele(context.Background(), &notification.SendTeleReq{
			ToNumber: NotiReq.ToNumber,
		})
	}
}

func (s *Server) SendSlack() gin.HandlerFunc {
	return func(c *gin.Context) {
		var NotiReq NotiReq
		err := c.BindJSON(&NotiReq)
		if err != nil {
			return
		}
		s.NotificationClient.SendSlack(context.Background(), &notification.SendSlackReq{
			To: NotiReq.To,
		})
	}
}

package main

import (
	"fmt"
	"github.com/go-redis/redis/v8"
	"notification_service/grpc"
	"sync"

	"log"
	"notification_service/common"
	"notification_service/server"
)

var wg *sync.WaitGroup

func main() {
	// create grpc server

	go grpc.RunGrpcServer()

	// Load config
	config := common.NewConfig()
	if err := config.Load("."); err != nil {
		log.Fatalln("cannot load config from env file", err)
	}

	//dbConn, err := gorm.Open(mysql.Open(config.DBConnectionURL()), &gorm.Config{})
	//if err != nil {
	//	log.Fatalln("cannot open database connection:", err)
	//}

	// create redis client
	rdb := redis.NewClient(&redis.Options{
		Addr:     fmt.Sprintf("redis:%v", config.RedisPort()),
		Password: config.RedisPassword(), // no password set
		DB:       0,                      // use default DB
	})

	// create connect GRPC
	notificationClient, err := common.GRPCConnect()

	if err != nil {
		log.Fatalf(" err while dial %v", err)
	}

	s := server.Server{
		Port:               config.AppPort(),
		AppEnv:             config.AppEnv(),
		RedisConn:          rdb,
		ServerReady:        make(chan bool),
		NotificationClient: notificationClient,
	}

	go func() {
		<-s.ServerReady
		close(s.ServerReady)
	}()

	s.Start()
}

package common

import (
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"log"
	notification "notification_service/proto"
)

func GRPCConnect() (notification.NotificationClient, error) {
	certFile := "ssl/server.crt"
	creds, err := credentials.NewClientTLSFromFile(certFile, "localhost")
	if err != nil {
		log.Fatalf(" err while dial %v", err)
		return nil, err
	}

	cc, err := grpc.Dial("localhost:50069", grpc.WithTransportCredentials(creds))

	if err != nil {
		log.Fatalf(" err while dial %v", err)
		return nil, err
	}

	return notification.NewNotificationClient(cc), nil
}
